# TinyGarden
  A simple raspberry pi project to automatically grow kitchen herbs at home.

## To start the project :

```
mongod --dbpath db
node server.js